﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Template.Application.Requests.CRUD
{
    public class GetByIdRequest
    {
        public int Id { get; set; }
    }
}
