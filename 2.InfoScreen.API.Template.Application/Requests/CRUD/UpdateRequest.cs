﻿using _4.InfoScreen.API.Template.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Template.Application.Requests.CRUD
{
    public class UpdateRequest<T> where T : DTOInterface
    {
        public T dto { get; set; }
    }
}
