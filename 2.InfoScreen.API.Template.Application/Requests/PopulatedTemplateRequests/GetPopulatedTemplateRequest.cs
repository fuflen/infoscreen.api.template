﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Template.Application.Requests.PopulatedTemplateRequests
{
    public class GetPopulatedTemplateRequest
    {
        public int TemplateId { get; set; }
        public int EndpointId { get; set; }
        public int NumberOfRows { get; set; }


    }
}
