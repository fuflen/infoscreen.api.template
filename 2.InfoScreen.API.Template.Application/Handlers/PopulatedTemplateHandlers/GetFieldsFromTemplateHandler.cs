﻿using _2.InfoScreen.API.Template.Application.Commands.PopulatedTemplateCommands;
using _2.InfoScreen.API.Template.Application.Commands.TemplateCommands;
using _2.InfoScreen.API.Template.Application.Managers.Interface;
using _2.InfoScreen.API.Template.Application.Validators.Interfaces;
using _2.InfoScreen.API.Template.Infrastructure.Interfaces.EntityRepositoryInterfaces;
using _3.InfoScreen.API.Template.Infrastructure.Services.Interfaces;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.DTO.External;
using _4.InfoScreen.API.Template.Domain.Models.Entities;
using _4.InfoScreen.API.Template.Domain.Models.Interfaces;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace _2.InfoScreen.API.Template.Application.Handlers.PopulatedTemplateHandlers
{
    public class GetFieldsFromTemplateHandler : IRequestHandler<GetFieldsFromTemplateCommand, Result<List<string>>>
    {
        private ITemplateRepostiory _repo;
        private IMapper _mapper;
        private ITemplateValidateManager _validateManager;
        private ILogger _logger;
        private ITemplateManager _templateManager;

        public GetFieldsFromTemplateHandler(
            ITemplateRepostiory repo,
            IMapper mapper,
            ITemplateValidateManager validateManager,
            ILogger<GetPopulatedTemplateHandler> logger,
            ITemplateManager templateManager)
        {
            _repo = repo;
            _mapper = mapper;
            _validateManager = validateManager;
            _logger = logger;
            _templateManager = templateManager;
        }

        public async Task<Result<List<string>>> Handle(GetFieldsFromTemplateCommand command, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidateGetByIdRequest(command.RequestModel);
                var template = await _repo.GetById(command.RequestModel.Id);

                var dto = _mapper.Map<TemplateDTO>(template);

                var result = _templateManager.GetFieldsFromTemplate(dto);

                if (result.HasError)
                {
                    return Result<List<string>>.Error(result.Exception);
                }

                return Result<List<string>>.Success(result.Data);
            }
            catch (Exception e)
            {
                if (e is ValidationException)
                {
                    _logger.LogError("Validation error with message: " + e.Message);
                }
                return Result<List<string>>.Error(e);
            }
        }
    }
}
