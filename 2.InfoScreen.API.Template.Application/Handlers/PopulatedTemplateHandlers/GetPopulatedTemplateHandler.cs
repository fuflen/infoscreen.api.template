﻿using _2.InfoScreen.API.Template.Application.Commands.PopulatedTemplateCommands;
using _2.InfoScreen.API.Template.Application.Commands.TemplateCommands;
using _2.InfoScreen.API.Template.Application.Managers.Interface;
using _2.InfoScreen.API.Template.Application.Validators.Interfaces;
using _2.InfoScreen.API.Template.Infrastructure.Interfaces.EntityRepositoryInterfaces;
using _3.InfoScreen.API.Template.Infrastructure.Services.Interfaces;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.DTO.External;
using _4.InfoScreen.API.Template.Domain.Models.Entities;
using _4.InfoScreen.API.Template.Domain.Models.Interfaces;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;


namespace _2.InfoScreen.API.Template.Application.Handlers.PopulatedTemplateHandlers
{
    public class GetPopulatedTemplateHandler : IRequestHandler<GetPopulatedTemplateCommand, Result<TemplateDTO>>
    {
        private ITemplateRepostiory _repo;
        private IMapper _mapper;
        private ITemplateValidateManager _validateManager;
        private ILogger _logger;
        private ITemplateManager _templateManager;
        private IMappedDataService _mappedDataService;

        public GetPopulatedTemplateHandler(
            ITemplateRepostiory repo,
            IMapper mapper,
            ITemplateValidateManager validateManager,
            ILogger<GetPopulatedTemplateHandler> logger,
            ITemplateManager templateManager,
            IMappedDataService mappedDataService)
        {
            _repo = repo;
            _mapper = mapper;
            _validateManager = validateManager;
            _logger = logger;
            _templateManager = templateManager;
            _mappedDataService = mappedDataService;
        }

        public async Task<Result<TemplateDTO>> Handle(GetPopulatedTemplateCommand command, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidateGetByViewEndpointRequest(command.RequestModel);
                var template = await _repo.GetById(command.RequestModel.TemplateId);

                var dto = _mapper.Map<TemplateDTO>(template);

                Result<List<MappedDynamicModelDTO>> mappedDataResult = await _mappedDataService.GetMappedDataFromEndpoint(command.RequestModel.EndpointId, command.RequestModel.TemplateId);

                var d = mappedDataResult.Data.FirstOrDefault();

                var result = _templateManager.PopulateTemplate(dto, mappedDataResult.Data);

                return Result<TemplateDTO>.Success(dto);
            }
            catch (Exception e)
            {
                if (e is ValidationException)
                {
                    _logger.LogError("Validation error with message: " + e.Message);
                }
                return Result<TemplateDTO>.Error(e);
            }
        }
    }
}
