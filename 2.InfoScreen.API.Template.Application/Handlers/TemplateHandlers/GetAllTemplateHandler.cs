﻿using _2.InfoScreen.API.Template.Application.Commands.TemplateCommands;
using _2.InfoScreen.API.Template.Infrastructure.Interfaces.EntityRepositoryInterfaces;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace _2.InfoScreen.API.Template.Application.Handlers.TemplateHandlers
{
    public class GetAllTemplateHandler : IRequestHandler<GetAllTemplateCommand, Result<List<TemplateDTO>>>
    {
        private ITemplateRepostiory _repo;
        private IMapper _mapper;
        private ILogger _logger;
        

        public GetAllTemplateHandler(
            ITemplateRepostiory repo,
            IMapper mapper,
            ILogger<GetAllTemplateHandler> logger)
        {
            _repo = repo;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Result<List<TemplateDTO>>> Handle(GetAllTemplateCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var endpoints = await Task.Factory.StartNew(() => _repo.GetAll().ToList());
                var endpointDtos = _mapper.Map<List<TemplateDTO>>(endpoints);
                return Result<List<TemplateDTO>>.Success(endpointDtos);
            }
            catch (Exception e)
            {
                if (e is ValidationException)
                {
                    _logger.LogError("Validation error with message: " + e.Message);
                }
                return Result<List<TemplateDTO>>.Error(e);
            }
        }

    }
}
