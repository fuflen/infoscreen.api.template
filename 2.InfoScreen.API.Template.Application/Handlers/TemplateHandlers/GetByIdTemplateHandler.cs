﻿using _2.InfoScreen.API.Template.Application.Commands.TemplateCommands;
using _2.InfoScreen.API.Template.Application.Managers.Interface;
using _2.InfoScreen.API.Template.Application.Validators.Interfaces;
using _2.InfoScreen.API.Template.Infrastructure.Interfaces.EntityRepositoryInterfaces;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.DTO.External;
using _4.InfoScreen.API.Template.Domain.Models.Entities;
using _4.InfoScreen.API.Template.Domain.Models.Interfaces;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;

namespace _2.InfoScreen.API.Template.Application.Handlers.TemplateHandlers
{
    public class GetByIdTemplateHandler : IRequestHandler<GetByIdTemplateCommand, Result<TemplateDTO>>
    {
        private ITemplateRepostiory _repo;
        private IMapper _mapper;
        private ITemplateValidateManager _validateManager;
        private ILogger _logger;
        ITemplateManager _templateManager;

        public GetByIdTemplateHandler(
            ITemplateRepostiory repo,
            IMapper mapper,
            ITemplateValidateManager validateManager,
            ILogger<GetByIdTemplateHandler> logger,
            ITemplateManager templateManager)
        {
            _repo = repo;
            _mapper = mapper;
            _validateManager = validateManager;
            _logger = logger;
            _templateManager = templateManager;
        }

        public async Task<Result<TemplateDTO>> Handle(GetByIdTemplateCommand command, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidateGetByIdRequest(command.RequestModel);
                var template = await _repo.GetById(command.RequestModel.Id);
                var dto = _mapper.Map<TemplateDTO>(template);

                
                return Result<TemplateDTO>.Success(dto);
            }
            catch (Exception e)
            {
                if (e is ValidationException)
                {
                    _logger.LogError("Validation error with message: " + e.Message);
                }
                return Result<TemplateDTO>.Error(e);
            }
        }
    }
}
