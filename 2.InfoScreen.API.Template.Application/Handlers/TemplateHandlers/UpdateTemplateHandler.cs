﻿using _2.InfoScreen.API.Template.Application.Commands.TemplateCommands;
using _2.InfoScreen.API.Template.Application.Validators.Interfaces;
using _2.InfoScreen.API.Template.Infrastructure.Interfaces.EntityRepositoryInterfaces;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.Entities;
using _4.InfoScreen.API.Template.Domain.Models.Interfaces;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;

namespace _2.InfoScreen.API.Template.Application.Handlers.TemplateHandlers
{
    class UpdateTemplateHandler : IRequestHandler<UpdateTemplateCommand, Result<TemplateDTO>>
    {
        private ITemplateRepostiory _repo;
        private IMapper _mapper;
        /// <see cref="IValidator"/>
        private ITemplateValidateManager _validateManager;
        private ILogger _logger;

        public UpdateTemplateHandler(
            ITemplateRepostiory repo,
            IMapper mapper,
            ITemplateValidateManager validateManager,
            ILogger<UpdateTemplateHandler> logger)
        {
            _repo = repo;
            _mapper = mapper;
            _validateManager = validateManager;
            _logger = logger;
        }

        public async Task<Result<TemplateDTO>> Handle(UpdateTemplateCommand command, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidateUpdateRequest(command.RequestModel);

                var endpoint = _mapper.Map<TemplateEntity>(command.RequestModel.dto);

                IEntity entity = await _repo.Update(endpoint.Id, endpoint);
                var dto = _mapper.Map<TemplateDTO>(entity);
                return Result<TemplateDTO>.Success(dto);
            }
            catch (Exception e)
            {
                if (e is ValidationException)
                {
                    _logger.LogError("Validation error with message: " + e.Message);
                }
                return Result<TemplateDTO>.Error(e);
            }
        }

    }
}
