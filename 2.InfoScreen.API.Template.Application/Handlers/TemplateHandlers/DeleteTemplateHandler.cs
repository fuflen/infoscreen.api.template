﻿using _2.InfoScreen.API.Template.Application.Commands.TemplateCommands;
using _2.InfoScreen.API.Template.Application.Validators.Interfaces;
using _2.InfoScreen.API.Template.Infrastructure.Interfaces.EntityRepositoryInterfaces;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.Entities;
using _4.InfoScreen.API.Template.Domain.Models.Interfaces;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using _3.InfoScreen.API.Template.Infrastructure.Services;

namespace _2.InfoScreen.API.Template.Application.Handlers.TemplateHandlers
{
    public class DeleteTemplateHandler : IRequestHandler<DeleteTemplateCommand, Result<TemplateDTO>>
    {
        private ITemplateRepostiory _repo;
        private IMapper _mapper;
        private ITemplateValidateManager _validateManager;
        private ILogger _logger;
        private RabbitMQTemplateService _messageService;


        public DeleteTemplateHandler(
            ITemplateRepostiory repo,
            IMapper mapper,
            ITemplateValidateManager validateManager,
            ILogger<DeleteTemplateHandler> logger,
            RabbitMQTemplateService messageService)
        {
            _repo = repo;
            _mapper = mapper;
            _validateManager = validateManager;
            _logger = logger;
            _messageService = messageService;
        }

        public async Task<Result<TemplateDTO>> Handle(DeleteTemplateCommand command, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidateDeleteRequest(command.RequestModel);
                IEntity entity = await _repo.Delete(command.RequestModel.Id);
                _messageService.SendTemplateDeletedMessage(command.RequestModel.Id);
                var dto = _mapper.Map<TemplateDTO>(entity);
                return Result<TemplateDTO>.Success(dto);
            }
            catch (Exception e)
            {
                if (e is ValidationException)
                {
                    _logger.LogError("Validation error with message: " + e.Message);
                }
                return Result<TemplateDTO>.Error(e);
            }
        }
    }
}
