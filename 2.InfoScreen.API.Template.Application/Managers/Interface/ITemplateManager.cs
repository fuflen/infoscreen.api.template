﻿using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.DTO.External;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Template.Application.Managers.Interface
{
    public interface ITemplateManager
    {
        Result<TemplateDTO> PopulateTemplate(TemplateDTO template, List<MappedDynamicModelDTO> data);
        Result<List<string>> GetFieldsFromTemplate(TemplateDTO template);
    }
}
