﻿using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.DTO.External;
using _4.InfoScreen.API.Template.Domain.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Scriban;
using _2.InfoScreen.API.Template.Application.Managers.Interface;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using Scriban.Runtime;
using System.Linq;
using System.Text.RegularExpressions;

namespace _2.InfoScreen.API.Template.Application.Managers
{
    public class TemplateManager : ITemplateManager
    {

        public Result<TemplateDTO> PopulateTemplate(TemplateDTO template, List<MappedDynamicModelDTO> data)
        {
            try
            {
                // Parse then given HTML content into a Template.
                var origTemplate = Scriban.Template.Parse(template.HTMLContent);

                var renderedTemplate = origTemplate.Render(new { data = data });
                template.HTMLContent = renderedTemplate;

                return Result<TemplateDTO>.Success(template);
            }
            catch (Exception e)
            {
                return Result<TemplateDTO>.Error(e);
            }
        }

        public Result<List<string>> GetFieldsFromTemplate(TemplateDTO template)
        {
            try
            {
                if (template.HTMLContent == null || !template.HTMLContent.Any())
                    throw new ArgumentNullException("No data was found.");

                var fields = new List<string>();
                string[] a = template.HTMLContent.Split(' ');
                
                // search for pattern in string 
                for (int i = 0; i < a.Length; i++)
                {
                    Regex reg = new Regex("model.dtomodel.field[0-9]*$");
                    // if match found increase count 
                    if (reg.IsMatch(a[i]))//a[i].Contains(" model.dtomodel.field[0-9]*$ "))
                    {
                        fields.Add(a[i].Split('.').Last());
                    }
                }
                if (!fields.Any())
                    throw new ArgumentNullException("No data was found.");

                return Result<List<string>>.Success(fields);
            }
            catch (Exception e)
            {
                return Result<List<string>>.Error(e);
            }
        }
    }
}
