﻿using _2.InfoScreen.API.Template.Application.Requests.PopulatedTemplateRequests;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Template.Application.Commands.PopulatedTemplateCommands
{
    public class GetPopulatedTemplateCommand : IRequest<Result<TemplateDTO>>
    {
        public GetPopulatedTemplateCommand(GetPopulatedTemplateRequest requestModel)
        {
            RequestModel = requestModel;
        }

        public GetPopulatedTemplateRequest RequestModel { get; set; }
    }
}
