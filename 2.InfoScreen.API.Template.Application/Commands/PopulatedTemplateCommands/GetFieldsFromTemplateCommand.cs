﻿using _2.InfoScreen.API.Template.Application.Requests.CRUD;
using _2.InfoScreen.API.Template.Application.Requests.PopulatedTemplateRequests;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Template.Application.Commands.PopulatedTemplateCommands
{
    public class GetFieldsFromTemplateCommand : IRequest<Result<List<string>>>
    {
        public GetFieldsFromTemplateCommand(GetByIdRequest requestModel)
        {
            RequestModel = requestModel;
        }

        public GetByIdRequest RequestModel { get; set; }
    }
}
