﻿using _4.InfoScreen.API.Template.Domain.Models.Result;
using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _2.InfoScreen.API.Template.Application.Requests.CRUD;

namespace _2.InfoScreen.API.Template.Application.Commands.TemplateCommands
{
    public class CreateTemplateCommand : IRequest<Result<TemplateDTO>>
    {
        public CreateTemplateCommand(CreateRequest<TemplateDTO> requestModel)
        {
            RequestModel = requestModel;
        }

        public CreateRequest<TemplateDTO> RequestModel { get; set; }
    }
}
