﻿using _2.InfoScreen.API.Template.Application.Requests.CRUD;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Template.Application.Commands.TemplateCommands
{
    public class GetAllTemplateCommand : IRequest<Result<List<TemplateDTO>>>
    {
        public GetAllTemplateCommand(GetAllRequest requestModel)
        {
            RequestModel = requestModel;
        }

        public GetAllRequest RequestModel { get; set; }
    }
}
