﻿using _2.InfoScreen.API.Template.Application.Requests.CRUD;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Template.Application.Commands.TemplateCommands
{
    public class GetByIdTemplateCommand : IRequest<Result<TemplateDTO>>
    {
        public GetByIdTemplateCommand(GetByIdRequest requestModel)
        {
            RequestModel = requestModel;
        }

        public GetByIdRequest RequestModel { get; set; }
    }
}
