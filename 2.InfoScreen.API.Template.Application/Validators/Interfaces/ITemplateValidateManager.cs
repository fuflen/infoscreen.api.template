﻿using _2.InfoScreen.API.Template.Application.Requests.CRUD;
using _2.InfoScreen.API.Template.Application.Requests.PopulatedTemplateRequests;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Template.Application.Validators.Interfaces
{
    public interface ITemplateValidateManager
    {
        void ValidateCreateRequest(CreateRequest<TemplateDTO> request);
        void ValidateUpdateRequest(UpdateRequest<TemplateDTO> request);
        void ValidateGetByIdRequest(GetByIdRequest request);
        void ValidateDeleteRequest(DeleteRequest request);
        void ValidateGetByViewEndpointRequest(GetPopulatedTemplateRequest request);


    }
}
