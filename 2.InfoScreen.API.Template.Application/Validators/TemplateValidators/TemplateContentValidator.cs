﻿using _4.InfoScreen.API.Template.Domain.ValidationMessages;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Template.Application.Validators.TemplateValidators
{
    public class TemplateContentValidator : AbstractValidator<string>
    {
        public TemplateContentValidator()
        {
            RuleFor(x => x).
                NotNull().
                WithMessage(ErrorMessages.TemplateContentNull).
                NotEmpty().
                WithMessage(ErrorMessages.TemplateContentEmpty).
                Must(BeValidHTMLContent).
                WithMessage(ErrorMessages.TemplateContentInvalid);
        }

        private static bool BeValidHTMLContent(string name)
        {
            return true;// name != null && name.Length >= 1 && name.Length <= 50;
        }
    }
}
