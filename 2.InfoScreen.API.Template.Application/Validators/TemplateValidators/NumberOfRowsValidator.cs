﻿using _4.InfoScreen.API.Template.Domain.ValidationMessages;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Template.Application.Validators.TemplateValidators
{
    public class NumberOfRowsValidator : AbstractValidator<int>
    {
        public NumberOfRowsValidator()
        {
            RuleFor(x => x).
                            NotNull().
                            WithMessage(ErrorMessages.IdNull).
                            GreaterThan(0).
                            WithMessage(ErrorMessages.NumberOfRowsToLow)
                            .LessThan(20).
                            WithMessage(ErrorMessages.NumberOfRowsToHigh);
        }
    }
}
