﻿using _2.InfoScreen.API.Template.Application.Requests.CRUD;
using _2.InfoScreen.API.Template.Application.Requests.PopulatedTemplateRequests;
using _2.InfoScreen.API.Template.Application.Validators.Interfaces;
using _2.InfoScreen.API.Template.Application.Validators.TemplateValidators;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Template.Application.Validators.Managers
{
    public class TemplateValidateManager : ITemplateValidateManager
    {
        /// <see cref="IValidator"/>
        private readonly IValidator<string> _contentValidator;
        private readonly IValidator<int> _idValidator;
        private readonly IValidator<int> _numberOfRowsVAlidator;

        public TemplateValidateManager()
        {
            _contentValidator = new TemplateContentValidator();
            _idValidator = new TemplateIdValidator();
            _numberOfRowsVAlidator = new NumberOfRowsValidator();
        }

        public void ValidateCreateRequest(CreateRequest<TemplateDTO> request)
        {
            _contentValidator.ValidateAndThrow(request.dto.HTMLContent);
        }

        public void ValidateDeleteRequest(DeleteRequest request)
        {
            _idValidator.ValidateAndThrow(request.Id);
        }

        public void ValidateGetByIdRequest(GetByIdRequest request)
        {
            _idValidator.ValidateAndThrow(request.Id);
        }

        public void ValidateUpdateRequest(UpdateRequest<TemplateDTO> request)
        {
            _idValidator.ValidateAndThrow(request.dto.Id);
            _contentValidator.ValidateAndThrow(request.dto.HTMLContent);
        }

        public void ValidateGetByViewEndpointRequest(GetPopulatedTemplateRequest request)
        {
            _idValidator.ValidateAndThrow(request.EndpointId);
            _idValidator.ValidateAndThrow(request.TemplateId);
            _numberOfRowsVAlidator.ValidateAndThrow(request.NumberOfRows);

        }
    }
}
