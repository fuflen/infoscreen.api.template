﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.Template.Domain.ValidationMessages
{
    public class ErrorMessages
    {
        // id
        public const string IdNull = "The id cannot be null.";
        public const string IdInvalid = "The id needs to be higher than 0";

        //Template Content
        public const string TemplateContentNull = "The HTML Content is required and cannot be null.";
        public const string TemplateContentEmpty = "Please enter some HTML.";
        public const string TemplateContentInvalid = "The given HTML content is invalid. Please re-enter.";

        public const string NumberOfRowsToLow = "The given number of rows must be above 0.";
        public const string NumberOfRowsToHigh = "The given number of rows must be below 20.";
    }
}
