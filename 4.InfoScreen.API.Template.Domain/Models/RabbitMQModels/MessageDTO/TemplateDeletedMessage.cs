﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.Template.Domain.Models.RabbitMQModels.MessageDTO
{
    public class TemplateDeletedMessage
    {
        public int TemplateId { get; set; }
    }
}
