﻿using _4.InfoScreen.API.Template.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.Template.Domain.Models.DTO.External
{
    public class MappedDynamicModelDTO : DTOInterface
    {
        public dynamic DTOModel { get; set; }
    }
}
