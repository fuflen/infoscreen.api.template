﻿using _4.InfoScreen.API.Template.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.Template.Domain.Models.DTO
{
    public class TemplateDTO : DTOInterface
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string HTMLContent { get; set; }
    }
}
