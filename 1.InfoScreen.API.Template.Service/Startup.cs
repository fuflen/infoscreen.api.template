﻿
using System;
using System.Net;
using _2.InfoScreen.API.Template.Infrastructure.Interfaces.EntityRepositoryInterfaces;
using _2.InfoScreen.API.Template.Infrastructure.Repositories.EntityRepos;
using _3.InfoScreen.API.Template.Infrastructure.Context;
using AutoMapper;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MediatR;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Swashbuckle.AspNetCore.Swagger;
using _2.InfoScreen.API.Template.Application.Validators.Interfaces;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _2.InfoScreen.API.Template.Application.Validators.Managers;
using _2.InfoScreen.API.Template.Application;
using _2.InfoScreen.API.Template.Application.CustomHealthChecks;
using _2.InfoScreen.API.Template.Application.Managers;
using _2.InfoScreen.API.Template.Application.Managers.Interface;
using _3.InfoScreen.API.Template.Infrastructure.Services.Interfaces;
using _3.InfoScreen.API.Template.Infrastructure.Services;
using _3.InfoScreen.API.Template.Infrastructure.HostedService;

namespace _1.InfoScreen.API.Template.Service
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder.WithOrigins("http://localhost:4200").AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
                });
            });

            //Register the Template DbContext in the ServiceCollection.
            services.AddDbContext<TemplateContext>(options =>
                options.UseSqlServer(Configuration["Connectionstring"])
            );

            //Register the TemplateRepository to the ServiceCollection
            services.AddScoped<ITemplateRepostiory,TemplateRepository>();

            services.AddHostedService<RabbitMQHostedService>();

            services.AddAutoMapper();
            // Documentation: https://dotnetcoretutorials.com/2017/09/23/using-automapper-asp-net-core/

            //Scans for handlers in project with the class Assembly
            services.AddMediatR(typeof(Assembly));

            services.AddSingleton<RabbitMQConnectionService>();

            services.AddHealthChecks()
                .AddCheck<MemoryHealthCheck>("Memory_check")
                .AddUrlGroup(uri =>
                {
                    uri.AddUri(new Uri("http://mappingservice/swagger"));
                    uri.ExpectHttpCode((int)HttpStatusCode.OK);
                    uri.UseGet();
                }, "mappingServiceConnection_check", HealthStatus.Unhealthy)
                .AddRabbitMQ(@"amqp://guest:guest@rabbitmq:5672", null, "rabbitMQConnection_check", HealthStatus.Unhealthy)
                .AddSqlServer(Configuration["Connectionstring"], "SELECT 1;", "SqlServerConnection_check", HealthStatus.Unhealthy);

            services.AddTransient<RabbitMQTemplateService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "Template API",
                    Description = "An API for storing Template information (HTML content)",
                    Version = "v1"
                });
            });

            //Register the Mapped service in the ServiceCollection.
            services.AddHttpClient<IMappedDataService, MappedDataService>();

            services.AddTransient<IDbInitializer, DbInitializer>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddScoped<ITemplateValidateManager, TemplateValidateManager>();
            services.AddScoped<ITemplateManager, TemplateManager>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            using (var scope = app.ApplicationServices.CreateScope())
            {
                // Initialize the database
                var services = scope.ServiceProvider;
                var dbContext = services.GetService<TemplateContext>();
                var dbInitializer = services.GetService<IDbInitializer>();
                dbInitializer.Initialize(dbContext);
            }


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //app.UseHttpsRedirection();

            app.UseSwagger();

            app.UseHealthChecks("/health", new HealthCheckOptions
            {
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse,
                Predicate = registration => true
            });

            app.UseCors(MyAllowSpecificOrigins);

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Template API v1");
            });

            app.UseMvc();
        }
    }
}
