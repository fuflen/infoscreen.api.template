﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _2.InfoScreen.API.Template.Application.Commands.TemplateCommands;
using _2.InfoScreen.API.Template.Application.Requests.CRUD;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace _1.InfoScreen.API.Template.Service.Controllers
{
    [Route("api/Template")]
    public class TemplateController : Controller
    {
        private IMediator _mediator;
        public TemplateController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<ActionResult<Result<List<TemplateDTO>>>> GetAll()
        {
            Result<List<TemplateDTO>> response = await _mediator.Send(new GetAllTemplateCommand(new GetAllRequest()));
            return Ok(response);
        }

        [HttpGet]
        [Route("GetById/{id}")]
        public async Task<ActionResult<Result<TemplateDTO>>> GetById([FromRoute]int id)
        {

            var response = await _mediator.Send(new GetByIdTemplateCommand(new GetByIdRequest() { Id = id }));
            return Ok(response);
        }

        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult<Result<TemplateDTO>>> Create([FromBody]TemplateDTO endpoint)
        {

            Result<TemplateDTO> response =
                await _mediator.Send(
                    new CreateTemplateCommand(
                        new CreateRequest<TemplateDTO>() { dto = endpoint }));
            return Ok(response);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<ActionResult<Result<TemplateDTO>>> Delete([FromRoute] int id)
        {
            Result<TemplateDTO> response = await _mediator.Send(new DeleteTemplateCommand(new DeleteRequest { Id = id }));
            return Ok(response);
        }

        [HttpPut]
        [Route("Update")]
        public async Task<ActionResult<Result<TemplateDTO>>> Update([FromBody] TemplateDTO endpoint)
        {
            Result<TemplateDTO> response = await _mediator.Send(new UpdateTemplateCommand(new UpdateRequest<TemplateDTO> { dto = endpoint }));
            return Ok(response);
        }
    }
}