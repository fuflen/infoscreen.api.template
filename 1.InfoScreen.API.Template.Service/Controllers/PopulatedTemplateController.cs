﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _2.InfoScreen.API.Template.Application.Commands.PopulatedTemplateCommands;
using _2.InfoScreen.API.Template.Application.Requests.CRUD;
using _2.InfoScreen.API.Template.Application.Requests.PopulatedTemplateRequests;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace _1.InfoScreen.API.Template.Service.Controllers
{
    [Route("api/PopulatedTemplate")]
    public class PopulatedTemplateController : Controller
    {
        private IMediator _mediator;
        public PopulatedTemplateController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("GetPopulatedTemplate/{templateId}/{endpointId}/{numberOfRows}")]
        public async Task<ActionResult<Result<TemplateDTO>>> GetPopulatedTemplate([FromRoute]int templateId, [FromRoute]int endpointId, [FromRoute]int numberOfRows)
        {
            var response = await _mediator.Send(
                new GetPopulatedTemplateCommand(
                    new GetPopulatedTemplateRequest() {
                        TemplateId = templateId,
                        EndpointId = endpointId,
                        NumberOfRows = numberOfRows }));

            return Ok(response);
        }


        [HttpGet]
        [Route("GetFieldsFromTemplate/{templateId}")]
        public async Task<ActionResult<Result<TemplateDTO>>> GetFieldsFromTemplate([FromRoute]int templateId)
        {
            var response = await _mediator.Send(
                new GetFieldsFromTemplateCommand(
                    new GetByIdRequest()
                    {
                        Id = templateId
                    }));

            return Ok(response);
        }
    }
}