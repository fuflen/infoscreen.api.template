﻿using _2.InfoScreen.API.Template.Application.Managers;
using _4.InfoScreen.API.Template.Domain.Models.DTO;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace _5.InfoScreen.API.Template.UnitTest.TemplateManagerTests.GetFieldsFromTemplateTests
{
    public class GetFieldsFromTemplateTests
    {
        [Theory]
        [MemberData(nameof(GetFieldsFromTemplateTestDataGenerator.GetValidData), MemberType = typeof(GetFieldsFromTemplateTestDataGenerator))]
        public void GetFieldsFromTemplate_ValidMemberData_MethodPassed(TemplateDTO DTO, List<string> expectedResult)
        {
            TemplateManager manager = new TemplateManager();

            var result = manager.GetFieldsFromTemplate(DTO);

            //Assert
            Assert.Equal(expectedResult, result.Data);
            //Assert.Collection(result,
            //    elem1 =>
            //    Assert.Equal(expectedResult[result.IndexOf(elem1)].DTOModel, elem1.DTOModel));
        }

        [Theory]
        [MemberData(nameof(GetFieldsFromTemplateTestDataGenerator.GetInvalidData), MemberType = typeof(GetFieldsFromTemplateTestDataGenerator))]
        public void GetFieldsFromTemplate_InvalidMemberData_ThrowException(TemplateDTO DTO, Exception expectedException)
        {
            TemplateManager manager = new TemplateManager();

            var result = manager.GetFieldsFromTemplate(DTO);

            //Assert
            var x = result.Exception.GetType().IsEquivalentTo(expectedException.GetType());
            Assert.True(x);
        }
    }
}
