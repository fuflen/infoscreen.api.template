﻿using _4.InfoScreen.API.Template.Domain.Models.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace _5.InfoScreen.API.Template.UnitTest.TemplateManagerTests.GetFieldsFromTemplateTests
{
    public class GetFieldsFromTemplateTestDataGenerator
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string HTMLContent { get; set; }

        static TemplateDTO ValidDTO = new TemplateDTO()
        {
            Id = 1,
            Name = "Card Template",
            HTMLContent = @"<ul id='data'>{{ for model in data }}<li><div class='card dataCard'><div class='card-header'>{{ model.dtomodel.field1 }}</div><div class='card-body'><h5 class='card-title'>{{ model.dtomodel.field2 }}</h5><p class='card-text'>{{ model.dtomodel.field3 }}</p><p>{{ model.dtomodel.field4 }}</p></div></div></li>{{ end }}</ul>"

        };

        static TemplateDTO InvalidDTO_NoFields = new TemplateDTO()
        {
            Id = 1,
            Name = "Card Template",
            HTMLContent = @"<ulid='data'>{{formodelindata}}<li><divclass='carddataCard'><divclass='card-header'>{{ field1 }}</div>"

        };

        static TemplateDTO InvalidDTO_NoData = new TemplateDTO()
        {
            Id = 1,
            Name = "Card Template",
            HTMLContent = @""

        };

        static TemplateDTO InvalidDTO_NullData = new TemplateDTO()
        {
            Id = 1,
            Name = "Card Template",
            HTMLContent = null

        };


        static List<string> ValidStringList = new List<string>()
        {
            "field1",
            "field2",
            "field3",
            "field4"
        };

        public static IEnumerable<object[]> GetValidData()
        {
            // Test Case 4
            yield return new object[] { ValidDTO, ValidStringList };
        }

        public static IEnumerable<object[]> GetInvalidData()
        {
            //yield return new object[] { new DynamicModelDTO() { DTOModel = "" }, new JsonReaderException() };
            // Test Case 3
            yield return new object[] { InvalidDTO_NoFields, new ArgumentNullException() };
            // Test Case 1
            yield return new object[] { InvalidDTO_NoData, new ArgumentNullException() };
            // Test Case 2
            yield return new object[] { InvalidDTO_NullData, new ArgumentNullException() };
        }
    }
}
