﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using _4.InfoScreen.API.Template.Domain.Models.Entities;

namespace _3.InfoScreen.API.Template.Infrastructure.Context
{
    public class TemplateContext : DbContext
    {
        /// <summary>
        /// Constructor for <see cref="IntegrationContext"/>
        /// </summary>
        /// <param name="options">The <see cref="DbContextOptions"/></param>
        public TemplateContext(DbContextOptions<TemplateContext> options)
           : base(options)
        {
        }

        public DbSet<TemplateEntity> TemplateModels { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<TemplateEntity>(entity =>
            {
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Id).ForSqlServerUseSequenceHiLo();
                entity.Property(x => x.HTMLContent).IsRequired();
            });
        }
    }
}
