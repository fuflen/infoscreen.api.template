﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3.InfoScreen.API.Template.Infrastructure.Context
{
    public interface IDbInitializer
    {
        void Initialize(TemplateContext context);
    }
}
