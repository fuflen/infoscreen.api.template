﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using _4.InfoScreen.API.Template.Domain.Models.Entities;

namespace _3.InfoScreen.API.Template.Infrastructure.Context
{
    public class DbInitializer : IDbInitializer
    {
        public void Initialize(TemplateContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            if (context.TemplateModels.Any())
            {
                return;
            }

            List<TemplateEntity> dataEndpoints = new List<TemplateEntity>
            {
                new TemplateEntity { Name = "Card Template",
                    HTMLContent = @"<ul id='data'>{{ for model in data }}<li><div class='card dataCard'><div class='card-header'>{{ model.dtomodel.field1 }}</div><div class='card-body'><h5 class='card-title'>{{ model.dtomodel.field2 }}</h5><p class='card-text'>{{ model.dtomodel.field3 }}</p><p>{{ model.dtomodel.field4 }}</p></div></div></li>{{ end }}</ul>"
                }
            };

            context.TemplateModels.AddRange(dataEndpoints);
            context.SaveChanges();
        }
    }
}
