﻿using _2.InfoScreen.API.Template.Infrastructure.Interfaces.EntityRepositoryInterfaces;
using _3.InfoScreen.API.Template.Infrastructure.Services;
using _4.InfoScreen.API.Template.Domain.Models.RabbitMQModels;
using _4.InfoScreen.API.Template.Domain.Models.RabbitMQModels.MessageDTO;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.Template.Infrastructure.HostedService
{
    public class RabbitMQHostedService : IHostedService
    {
        private IServiceProvider _services;
        private RabbitMQConnectionModel _connectionModel;

        private const string DeleteEndpointBindingKey = "mapping.delete.failed.template";
        private const string QueueName = "undo_delete_template";

        public RabbitMQHostedService(RabbitMQConnectionService connection, IServiceProvider services)
        {
            _services = services;
            _connectionModel = connection.GetConnectionModel();
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _connectionModel.Model.QueueDeclare(QueueName, true, false, false, null);

            _connectionModel.Model.QueueBind(QueueName, _connectionModel.ExchangeName, DeleteEndpointBindingKey);

            var consumer = new EventingBasicConsumer(_connectionModel.Model);

            consumer.Received += async (model, ea) =>
            {
                await HandleReceivedEvent(ea);
                var body = ea.Body;
                var routingKey = ea.RoutingKey;
                var message = Encoding.UTF8.GetString(body);

            };

            _connectionModel.Model.BasicConsume(queue: QueueName,
                autoAck: true,
                consumer: consumer);
        }

        private async Task HandleReceivedEvent(BasicDeliverEventArgs ea)
        {
            switch (ea.RoutingKey)
            {
                case DeleteEndpointBindingKey:
                    TemplateDeletedMessage endpointMessage = Deserialize<TemplateDeletedMessage>(ea.Body);
                    await UndoDeleteTemplate(endpointMessage.TemplateId);
                    break;
            }

        }



        public static T Deserialize<T>(byte[] data) where T : class
        {
            using (var stream = new MemoryStream(data))
            using (var reader = new StreamReader(stream, Encoding.UTF8))
                return JsonSerializer.Create().Deserialize(reader, typeof(T)) as T;
        }

        private async Task UndoDeleteTemplate(int templateId)
        {
            Console.WriteLine("Undoing delete of template: " + templateId);
            try
            {
                using (var scope = _services.CreateScope())
                {
                    var templateRepository =
                        scope.ServiceProvider
                            .GetRequiredService<ITemplateRepostiory>();

                    var entities = await templateRepository.UndoDelete(templateId);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                _connectionModel.Model.Dispose();
                cancellationToken.ThrowIfCancellationRequested();
            });
        }
    }
}
