﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3.InfoScreen.API.Template.Infrastructure.Services.Interfaces
{
    public interface IMessageGateway
    {
        void SendTemplateDeletedMessage(int templateId);


    }
}
