﻿using _4.InfoScreen.API.Template.Domain.Models.DTO.External;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.Template.Infrastructure.Services.Interfaces
{
    public interface IMappedDataService
    {
        Task<Result<List<MappedDynamicModelDTO>>> GetMappedDataFromEndpoint(int endpointId, int templateId);
    }
}
