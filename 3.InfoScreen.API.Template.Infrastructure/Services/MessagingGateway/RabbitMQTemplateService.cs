﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;
using _4.InfoScreen.API.Template.Domain.Models.RabbitMQModels;
using _4.InfoScreen.API.Template.Domain.Models.RabbitMQModels.MessageDTO;
using _3.InfoScreen.API.Template.Infrastructure.Services.Interfaces;

namespace _3.InfoScreen.API.Template.Infrastructure.Services
{
    public class RabbitMQTemplateService : IMessageGateway
    {
        private static ConnectionFactory _factory;
        private static IModel _model;
        private static RabbitMQConnectionModel _connectionModel;

        private const string DeleteRoutingKey = "template.deleted";
        public RabbitMQTemplateService(RabbitMQConnectionService connection)
        {
            _connectionModel = connection.GetConnectionModel();
        }

        public void SendTemplateDeletedMessage(int templateId)
        {
            var messageModel = new TemplateDeletedMessage()
            {
                TemplateId = templateId,
            };
            byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(messageModel));
            SendMessage(body, DeleteRoutingKey);
        }

        private void SendMessage(byte[] message, string routingKey)
        {
            _connectionModel.Model.BasicPublish(_connectionModel.ExchangeName, routingKey, _connectionModel.Properties, message);
        }
    }
}
