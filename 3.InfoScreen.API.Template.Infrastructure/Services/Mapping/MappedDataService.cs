﻿using _3.InfoScreen.API.Template.Infrastructure.Services.Interfaces;
using _4.InfoScreen.API.Template.Domain.Models.DTO.External;
using _4.InfoScreen.API.Template.Domain.Models.Result;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.Template.Infrastructure.Services
{
    public class MappedDataService : IMappedDataService
    {
        private HttpClient _client;
        private string mappingServiceUrl = "http://mappingservice/api/MappedData/GetMappedData";
        public MappedDataService(HttpClient client)
        {
            this._client = client;
        }
        

        public async Task<Result<List<MappedDynamicModelDTO>>> GetMappedDataFromEndpoint(int endpointId, int templateId)
        {
            HttpResponseMessage response = await _client.GetAsync(mappingServiceUrl + "/" + endpointId + "/" + templateId);

            var dto = await response.Content.ReadAsAsync<Result<List<MappedDynamicModelDTO>>>();

            return dto;
        }
    }
}
