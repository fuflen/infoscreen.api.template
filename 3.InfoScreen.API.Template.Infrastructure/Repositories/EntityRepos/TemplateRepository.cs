﻿using _2.InfoScreen.API.Template.Infrastructure.Interfaces.EntityRepositoryInterfaces;
using _3.InfoScreen.API.Template.Infrastructure.Context;
using _4.InfoScreen.API.Template.Domain.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Template.Infrastructure.Repositories.EntityRepos
{
    public class TemplateRepository : GenericRepository<TemplateEntity>, ITemplateRepostiory
    {
        public TemplateRepository(TemplateContext dbContext)
        : base(dbContext)
        {
            dbContext.Database.EnsureCreated();
        }
    }
}
