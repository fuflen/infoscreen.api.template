﻿using _4.InfoScreen.API.Template.Domain.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Template.Infrastructure.Interfaces.EntityRepositoryInterfaces
{
    public interface ITemplateRepostiory : IGenericRepository<TemplateEntity>
    {
    }
}
